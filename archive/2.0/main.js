//start the title track tile shower when the page is ready
document.addEventListener("DOMContentLoaded", function(){
	getMetaData();

	//query the doc tags
	audioTag = document.querySelector("audio");
	button = document.querySelector("#playPause")
	volume = document.querySelector("#volume")

	//setup the volume slider
	volume.addEventListener("input", function() {
		audioTag.volume = volume.value / 100;
	})

	//setup the play button
	button.addEventListener("click", function(){
		if (audioTag.paused) {
			audioTag.load();
			audioTag.play();
		} else {
			audioTag.pause();
		}
		updateButton();
	})

	//setup the play button auto update
	audioTag.addEventListener("playing", updateButton);
	audioTag.addEventListener("pause", updateButton);
	
	// I HAVE NO CLUE HOW THIS FIXES THE SAFARI BUG BUT WHATEVER IT WORKS NOW
	// setTimeout(startPlayback,500);
	// ??NO??
  // Just assume it won't work on safari. I mean what are the chances that 
  // someone will be streaming from safari and also not have OPENGL2 support.
})

//button update function
function updateButton() {
	console.log("yes")
	button = document.querySelector("#playPause")
	if (audioTag.paused) {
		button.innerHTML = "Play"
	} else {
		button.innerHTML = "Pause"
	}
}
//load the audio and play it to get around the safari infiload bug
function startPlayback() {
	audioTag = document.querySelector("audio");
	audioTag.load();
	audioTag.play();
}

