const radioSource = "https://icecast.mocrd.org/live";

const resetAudio = "javascript:void(0)";

const loader = document.getElementById('loader');

const audio = document.getElementById('audio');

audio.addEventListener('loadstart', () => {
  if (audio.src !== resetAudio) {
    loader.style.visibility = "visible";
  }
});

audio.addEventListener('playing', () => {
  loader.style.visibility = "hidden";
});

document.getElementById('play').addEventListener('click', (evt) => {
  audio.src = radioSource;
  audio.play();
	document.getElementById('stop').style.visibility = "visible";
	document.getElementById('play').style.visibility = "hidden";
})

document.getElementById('stop').addEventListener('click', (evt) => {
  audio.src = resetAudio;
  audio.pause();
  audio.load();
	document.getElementById('stop').style.visibility = "hidden";
	document.getElementById('play').style.visibility = "visible";

})


