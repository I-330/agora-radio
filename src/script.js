const radioSource = "http://us-icecast.agora.radio/mpd";

const resetAudio = "javascript:void(0)";

const loader = document.getElementById('loader');

const audio = document.getElementById('audio');

audio.addEventListener('loadstart', () => {
  if (audio.src !== resetAudio) {
    loader.style.visibility = "visible";
  }
});

audio.addEventListener('playing', () => {
  loader.style.visibility = "hidden";
});

document.getElementById('play').addEventListener('click', (evt) => {
  audio.src = radioSource;
  audio.play();
	document.getElementById('stop').style.visibility = "visible";
	document.getElementById('play').style.visibility = "hidden";
})

document.getElementById('stop').addEventListener('click', (evt) => {
  audio.src = resetAudio;
  audio.pause();
  audio.load();
	document.getElementById('stop').style.visibility = "hidden";
	document.getElementById('play').style.visibility = "visible";
  loader.style.visibility = "hidden";

})









//Make the DIV element draggagle:
var items = document.getElementsByClassName("dragdiv");

for (var i = 0; i < items.length; ++i) {
  centerElement(items[i],i*30);
  dragElement(items[i]);
}

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  elmnt.childNodes[1].onmousedown = dragMouseDown;

  function dragMouseDown(e) {
    e = e || window.event;
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    
    //jquery z-index management [Causes a lot of errors so I removed it]
    /*elmnt.style.zIndex = items.length;
    for (var i = 0; i < items.length; ++i) {
      if(i != $(elmnt).index() ) {
        items[i].style.zIndex = elmnt.style.zIndex - 1;
      }
    }*/
  }

  function closeDragElement() {
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

function centerElement(elmnt, offset) {
  elmnt.style.top = document.body.clientHeight/2 - elmnt.clientHeight/2 - offset + "px";;
  elmnt.style.left = document.body.clientWidth/2 - elmnt.clientWidth/2 + offset +"px";
}
