<!DOCTYPE html>
<html>
<head>
    <!-- Metadata & SEO -->
    <title>Agora Radio</title>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width"/>
    <meta name="description" content="Agora Road Radio Station">
    <meta name="keywords" content="radio,vaporwave,">

    <meta property="og:title" content="Agora Radio">
    <meta property="og:url" content="https://radio.mocrd.org">
    <meta property="og:site_name" content="Agora Radio">
    <meta property="og:description" content="Agora Road vaporwave radio station">

    <!-- Page Resources -->
    <link rel="stylesheet" href="/style.css">
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/fav/favicon-16x16.png">
</head>

<body>
    <header>
        <img id="imac_icon" src="img/apple.png">
        <div class="taskbar_spacer"></div>
        <button id="taskbar_file">
            <div class="taskbar_item prevent-select" id="file">File</div>
        </button>
        <div class="taskbar_spacer"></div>
        <button id="taskbar_file">
        <div class="taskbar_item prevent-select" id="file">About</div>
        </button>
        <div class="taskbar_spacer"></div>
        <button id="taskbar_file">
        <div class="taskbar_item prevent-select" id="file">View</div>
        </button>
        <div class="taskbar_spacer"></div>
        <button id="taskbar_file">
        <div class="taskbar_item prevent-select" id="file">Radio</div>
        </button>
    </header>

    <div class="dragdiv">
        <div class="dragdivheader">
            <div class="header_bars" id="header_bars_short"></div>
            <div class="header_square"></div>
            <div class="header_bars"></div>
            <div class="header_title prevent-select">Macintosh Cafe</div>
            <div class="header_bars" id="header_bars_right"></div>

        </div>
        <div class="dragdiv_window">

            <div id="media-box">
                <audio id="audio">Your browser doesn't support the audio element. :(</audio>
	
                <button id="play">
                    <img id="play_btn" src="img/play.png">
                </button>
	
                <button id="stop">
                    <img id="stop_btn" src="img/pause.png">
                </button>
  	  
                <h1 id="loader">
                    Loading...
                </h1>
            </div>
            <!--         
            <div class="apple_slider">
                <input type="range" orient="vertical" min="1" max="100" value="50" class="slider" id="apple_slider_range">
            </div>
            <h1>Message is what matters</h1>
            -->
        </div>
    </div>


</body>
<script src="script.js"></script>
</html>
