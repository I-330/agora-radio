# Agora-Radio

## About

This is the source code for the Agora Radio website. Feel free to steal any of
it and use it in your own projects. Most of the JS is unreadable, and the media
players only sometimes work. 

## Checklist

- [#] [Add support for deviced without OpenGL2]
- [#] [Backup rtmp link for people with js disabled] 
- [#] [Archive Directory with Dates]
- [#] [Add lynx support]
- [ ] [Old designs archive]
- [ ] [Credits Page]
- [ ] [Enable check for surf-browser]
- [ ] [Tor mirror]

## License
WTFPL - DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
